
using PyPlot
include("../beziertools.jl")

dim_p = 1
dim_s = 1
p = 5
n = 6
if( mod(n+p+1,2) != 0 )
    n -= 1
end
c_pts = reshape(collect(linspace(0,1,n)),1,n)
knot_v = [zeros(div(n+p+1,2));ones(div(n+p+1,2))]
geometry = Bezier( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts )
h = 0.001

# Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    b = BezierBasis(i,geometry.order[1],geometry.knot_v[1],tb)
    plot(tb,b)
end
title("B-Spline Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
#ylim([-0.5,1.5])


# Del_Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    b = DelBezierBasis(i,geometry.order[1],geometry.knot_v[1],tb)
    plot(tb,b)
end
title("First-Derivative of B-Spline Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
#ylim([-0.5,1.5])






dim_p = 1
dim_s = 1
p = 5
n = 6
if( mod(n+p+1,2) != 0 )
    n -= 1
end
c_pts = reshape(collect(linspace(0,1,n)),1,n)
knot_v = [zeros(div(n+p+1,2));ones(div(n+p+1,2))]
weights = (rand(n) .* 2.0 + 0.5)
geometry = Nurbs( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts, weights )
h = 0.001

# Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    b = NurbsBasis(i,geometry.order[1],geometry.knot_v[1],weights,tb)
    plot(tb,b)
end
title("NURBS Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
#ylim([-0.5,1.5])


# Del_Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    b = DelNurbsBasis(i,geometry.order[1],geometry.knot_v[1],weights,tb)
    plot(tb,b)
end
title("First-Derivative of NURBS Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
#ylim([-0.5,1.5])
