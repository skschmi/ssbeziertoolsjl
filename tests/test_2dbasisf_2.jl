


using PyPlot; plt = PyPlot;
include("../beziertools.jl")
using SSBezierTools
import SSBezierTools.Nurbs
import SSBezierTools.NurbsBasis
import SSBezierTools.domain_X
import SSBezierTools.draw
import SSBezierTools.insertKnot

dim_p = 1
dim_s = 2

p1 = 3
knot_v = Array[[0., 0, 0, 0, 1, 1, 1, 1]]
n1 = 4
span1 = n1-1

c_pts = zeros(dim_s,n1)
c_pts[:,1] = [0.,0]
c_pts[:,2] = [0,1.5]
c_pts[:,3] = [4,2]
c_pts[:,4] = [4.5,4]

weights = Array[[1., 1., 1., 1.]]
#weights = Array[[1., 3., 4., 1.]]

self = Nurbs(dim_p,
            dim_s,
            [p1],
            knot_v,
            [n1-1],
            c_pts,
            weights)

res = [50]
X,t,h = draw(self,res)

plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
plt.hold(false)
plt.xlim(-2,6)
plt.ylim(-1,5)
#plt.savefig("test1.pdf")
#plt.savefig("test2.pdf")

#  Testing inserting a knot
insertKnot(self,1,0.25)

res = [50]
X,t,h = draw(self,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
plt.hold(false)
plt.xlim(-2,6)
plt.ylim(-1,5)
