#Purpose: Make sure the "BernsteinBasis" function looks like it's working.

using GR
include("../beziertools.jl")

xi = collect(0:0.01:1)
plot(xi,BernsteinBasis(3,2,xi),title="BernsteinBasis Test")
