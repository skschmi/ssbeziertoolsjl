#Purpose: Make sure the "BezierBasis" function looks like it's working.
#         See if I can plot it in "Plots" using GR as the backend.

using Plots
include("../beziertools.jl")
gr()

t = collect(0:0.01:5)
knot_v = [0,0,0,1,1,2,2,3,3,4,4,4]
order = 2

plot(t,BezierBasis(1,order,knot_v,t))
for i = 2:length(knot_v)-1
    plot!(t,BezierBasis(i,order,knot_v,t))
end
plot!(t,BezierBasis(length(knot_v),order,knot_v,t))
