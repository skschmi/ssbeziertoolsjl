


using PyPlot; plt = PyPlot;
include("../beziertools.jl")
using SSBezierTools
import SSBezierTools.Nurbs
import SSBezierTools.NurbsBasis
import SSBezierTools.domain_X
import SSBezierTools.draw
import SSBezierTools.insertKnot

dim_p = 2
dim_s = 2

p1 = 2
p2 = 3

knot_v = Array[[0., 0, 0, 1, 1, 1],[0., 0, 0, 0, 1, 1, 1, 1]]

n1 = 3
n2 = 4

span1 = n1-1
span2 = n2-1

c_pts = zeros(dim_s,n1,n2)
c_pts[:,1,1] = [0.,0]
c_pts[:,2,1] = [1.,0]
c_pts[:,3,1] = [2.,0]
c_pts[:,1,2] = [-0.5,1]
c_pts[:,2,2] = [1.,1]
c_pts[:,3,2] = [2.5,1]
c_pts[:,1,3] = [-0.5,2]
c_pts[:,2,3] = [1.,2]
c_pts[:,3,3] = [2.5,2]
c_pts[:,1,4] = [0.,3]
c_pts[:,2,4] = [1.,3]
c_pts[:,3,4] = [2.,3]

weights = Array[[1., 2., 1.],[1., 1.3, 1.4, 1.]]
#weights = Array[[1., 1., 1.],[1., 1., 1., 1.]]

self = Nurbs(dim_p,
            dim_s,
            [p1,p2],
            knot_v,
            [n1-1,n2-1],
            c_pts,
            weights)

res = [50,50]
X,t,h = draw(self,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
plt.hold(false)
plt.xlim(-1,3)
plt.ylim(-0.5,3.5)

#  Testing inserting a knot in 2D!
insertKnot(self,1,0.25)

res = [50,50]
X,t,h = draw(self,res)
plt.figure()
plt.scatter(X[1,:],X[2,:],color="b")
plt.hold(true)
plt.scatter(self.control_pts[1,:],self.control_pts[2,:],color="r")
plt.hold(false)
plt.xlim(-1,3)
plt.ylim(-0.5,3.5)
