#Purpose: Make sure the "BezierBasis" function looks like it's working.
#         See if I can plot it with GR.



using GR
include("../beziertools.jl")
t = collect(0:0.01:5)
knot_v = [0,0,0,0,1,1,2,2,3,3,4,4,4,4]
order = 3
for i in 1:length(knot_v)
    plot(t,BezierBasis(i,order,knot_v,t))
    hold(true)
end
hold(false)
title("BezierBasis Test")



#=
command_str = "plot("
for i in 1:length(knot_v)
    command_str = command_str*"t,BezierBasis("*string(i)*",order,knot_v,t),\"b\","
end
command_str = command_str*")"
eval(parse(command_str))
title("BezierBasis Test")
=#


#=
plot(t,BezierBasis(1,order,knot_v,t),"b",
     t,BezierBasis(2,order,knot_v,t),"b",
     t,BezierBasis(3,order,knot_v,t),"b",
     t,BezierBasis(4,order,knot_v,t),"b",
     t,BezierBasis(5,order,knot_v,t),"b",
     t,BezierBasis(6,order,knot_v,t),"b",
     t,BezierBasis(7,order,knot_v,t),"b",
     t,BezierBasis(8,order,knot_v,t),"b",
     t,BezierBasis(9,order,knot_v,t),"b",
     t,BezierBasis(10,order,knot_v,t),"b",
     t,BezierBasis(11,order,knot_v,t),"b",
     t,BezierBasis(12,order,knot_v,t),"b")
title("BezierBasis Test")
=#
