# Purpose: Visualizing the results of my knot-insertion algorithm
#          to make sure it's correct.

include("../beziertools.jl")

knot_v = [0,0,0,0,1,1,1,1]
control_pts = [0. 3. 1. 1.;
               1. 1. 2. 0.]
weights = [1,1,1,1]
order = 3
spans = size(control_pts)[2]-1
new_knot = 0.5

new_knot_v,new_control_pts = InsertKnot(new_knot,order[1],knot_v,control_pts)
new_weights = [1,1,1,1,1]
new_spans = size(new_control_pts)[2]-1

#                    dim_p, dim_s, order,    knot_v,            spans,       control_pts,     weights,      res
X, t, h  = NurbsDraw(1,     2,     [order],  Array[knot_v],     [spans],     control_pts,     weights,      100)
Xn,tn,hn = NurbsDraw(1,     2,     [order],  Array[new_knot_v], [new_spans], new_control_pts, new_weights,  100)

using PyPlot

#Original nurbs spline
figure()
scatter(X[1,:],X[2,:])
scatter(control_pts[1,:],control_pts[2,:],color="r")
title("Original nurbs spline")
xlim([-0.5,3.5])
ylim([-0.5,2.5])

# Original basis functions
figure()
for i = 1:spans+1
    tb = minimum(knot_v):0.01:maximum(knot_v)
    b = NurbsBasis(i,order,knot_v,weights,tb)
    plot(tb,b)
end
title("Original basis functions")
xlim([minimum(knot_v),maximum(knot_v)])
ylim([-0.5,1.5])

#Knot-inserted nurbs spline
figure()
scatter(Xn[1,:],Xn[2,:])
scatter(new_control_pts[1,:],new_control_pts[2,:],color="r")
title("Knot-inserted nurbs spline")
xlim([-0.5,3.5])
ylim([-0.5,2.5])

# Knot-inserted basis functions
figure()
for i = 1:new_spans+1
    tb = minimum(knot_v):0.01:maximum(new_knot_v)
    b = NurbsBasis(i,order,new_knot_v,new_weights,tb)
    plot(tb,b)
end
title("Knot-inserted basis functions")
xlim([minimum(new_knot_v),maximum(new_knot_v)])
ylim([-0.5,1.5])
