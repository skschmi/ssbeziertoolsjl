# Purpose: Experimenting with moving a knot in a knot value,
#          and seeing how it chagnes the spline.
using PyPlot
include("../beziertools.jl")

knot_v = [0.,0,0,0,1,3,4,4,4,4]
control_pts = [0. 1. 9. 7. 3  -0.5;
               0  3  5 -3 -3  -1.5]
weights = [1.,1,1,1,1,1]
order = 3
spans = size(control_pts)[2]-1

#                    dim_p, dim_s, order,   knot_v,            spans,       control_pts,     weights,      res
X, t, h  = NurbsDraw(1,     2,     [order], Array[knot_v],     [spans],     control_pts,     weights,      100)
knot_v[5] = 0.3
Xn,tn,hn = NurbsDraw(1,     2,     [order], Array[knot_v],     [spans],     control_pts,     weights,      100)



#Original nurbs spline
figure()
scatter(X[1,:],X[2,:],color="b")
scatter(control_pts[1,:],control_pts[2,:],color="r")
title("Original nurbs spline")
xlim([-1,10])
ylim([-4,7])

#Second nurbs spline
figure()
scatter(Xn[1,:],Xn[2,:],color="b")
scatter(control_pts[1,:],control_pts[2,:],color="r")
title("Moved-knot nurbs spline")
xlim([-1,10])
ylim([-4,7])
