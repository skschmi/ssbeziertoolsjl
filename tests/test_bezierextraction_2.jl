
using PyPlot
include("../beziertools.jl")

#1./3  2./3

#=
Note about creating a Bezier Curve, where knot vector is strictly 0s and 1s:
    Four things have to be consistent with each other:
        order
        spans
        control points
        knot vector.
    These are the rules:
        order = p
        length(control_pts) = n
        n >= p+1 (for knot_v has just 0s and 1s, n == p+1)
        spans = n-1
        length(knot_v) = p+n+1
        multiplicity(knot_v[1]) == multiplicity(knot_v[end])
        multiplicity(knot_v[1]), multiplicity(knot_v[end]) > max(multiplicity of interior) <- ???
=#

dim_p = 1
dim_s = 1
p = 3
n = 5
c_pts = [0. 0.25 0.5 0.75 1.0]
knot_v = [0,0,0,0,.5,1,1,1,1]

geometry = Bezier( dim_p, dim_s, [p], Array[knot_v], [n-1], c_pts)


X,t,h = draw(geometry,500)

# Original Nurbs spline
figure()
scatter( X, zeros(length(X)) )
scatter(geometry.control_pts, zeros(length(geometry.control_pts)), color="r")
title("Original NURBS spline")
xlim([0,1])
ylim([-0.5,0.5])


# Original Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    #b = NurbsBasis(i,geometry.order[1],geometry.knot_v[1],ones(geometry.spans[1]+1),tb)
    b = BezierBasis(i,geometry.order[1],geometry.knot_v[1],tb)
    plot(tb,b)
end
title("Original Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
ylim([-0.5,1.5])


# Print to screen all the coeffs in the de-boor algorithm
the_p = p
for ii = 1:length(c_pts)
    for pi = the_p:-1:0
        xi = 0.0
        ans_top = (xi-knot_v[ii])/(knot_v[ii+pi]-knot_v[ii])
        ans_bot = (knot_v[ii+pi+1]-xi)/(knot_v[ii+pi+1]-knot_v[ii+1])
        print("(i,p)=(",ii,",",pi,"):(",ans_top,",",ans_bot,")  ")
    end
    the_p -= 1
    print("\n")
end


# Bezier Extraction -- adding the knots
insertKnot(geometry,0.5)
insertKnot(geometry,0.5)


X,t,h = draw(geometry,500)

# Bezier-Extracted Nurbs spline
figure()
scatter( X, zeros(length(X)) )
scatter(geometry.control_pts, zeros(length(geometry.control_pts)), color="r")
title("Bezier-Extracted B-spline spline")
xlim([0,1])
ylim([-0.5,0.5])

# Bezier-Extracted Basis functions
figure()
for i = 1:geometry.spans[1]+1
    tb = minimum(geometry.knot_v[1]):h:maximum(geometry.knot_v[1])
    b = BezierBasis(i,geometry.order[1],geometry.knot_v[1],tb)
    plot(tb,b)
end
title("Bezier-Extracted Basis functions")
xlim([minimum(geometry.knot_v[1]),maximum(geometry.knot_v[1])])
ylim([-0.5,1.5])
