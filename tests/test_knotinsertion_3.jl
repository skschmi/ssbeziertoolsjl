# Purpose:
# Test the NURBS knot-insertion algorithm.

using PyPlot
include("../beziertools.jl")

knot_v = [0,0,0,0,1,3,4,4,4,4]
control_pts = [0. 1. 9. 7. 3  -0.5;
               0  3  5 -3 -3  -1.5]
weights = [1,2.5,7,4,3.5,1]
order = 3
spans = size(control_pts)[2]-1
new_knot = 2

#                 dim_p, dim_s, order,   knot_v,       spans,   control_pts, weights
nurbsgeom = Nurbs(1,     2,     [order], Array[knot_v],[spans], control_pts, weights)

X, t, h  = draw(nurbsgeom,201)

#Original nurbs spline
figure()
scatter(X[1,:],X[2,:])
scatter(nurbsgeom.control_pts[1,:],nurbsgeom.control_pts[2,:],color="r")
title("Original nurbs spline")
xlim([-1,10])
ylim([-4,7])

# Original basis functions
figure()
for i = 1:nurbsgeom.spans[1]+1
    tb = minimum(nurbsgeom.knot_v[1]):0.01:maximum(nurbsgeom.knot_v[1])
    b = NurbsBasis(i,nurbsgeom.order[1],nurbsgeom.knot_v[1],nurbsgeom.weights,tb)
    plot(tb,b)
end
title("Original basis functions")
xlim([minimum(nurbsgeom.knot_v[1]),maximum(nurbsgeom.knot_v[1])])
ylim([-0.5,1.5])




insertKnot(nurbsgeom,new_knot)
Xn,tn,hn = draw(nurbsgeom,201)

#Knot-inserted nurbs spline
figure()
scatter(Xn[1,:],Xn[2,:])
scatter(nurbsgeom.control_pts[1,:],nurbsgeom.control_pts[2,:],color="r")
title("Knot-inserted nurbs spline")
xlim([-1,10])
ylim([-4,7])

# Knot-inserted basis functions
figure()
for i = 1:nurbsgeom.spans[1]+1
    tb = minimum(nurbsgeom.knot_v[1]):0.01:maximum(nurbsgeom.knot_v[1])
    b = NurbsBasis(i,nurbsgeom.order[1],nurbsgeom.knot_v[1],nurbsgeom.weights,tb)
    plot(tb,b)
end
title("Knot-inserted basis functions")
xlim([minimum(nurbsgeom.knot_v[1]),maximum(nurbsgeom.knot_v[1])])
ylim([-0.5,1.5])
