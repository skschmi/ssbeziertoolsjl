
function clock()
    t = ccall( (:clock, "libc"), Int32, ())
    return t
end

clock_t = clock()
@show clock_t


function factorial(n::Int64)
    #integer(8) function factorial(a) result(ans)
    functionptr = cglobal( (:__basis_mod_MOD_factorial,"libfbasis.dll") )
    factorial_result = ccall( functionptr, Int64, (Ptr{Int64},), &n)
    #factorial_result = ccall( (:__basis_mod_MOD_factorial,"libfbasis.dll"), Int64, (Ptr{Int64},), &n)
    return factorial_result
end

factorial_result = factorial(6)
@show factorial_result


function binomial(n::Int64,k::Int64)
    #integer(8) function binomial(n,k) result(ans)
    binomial_result = ccall( (:__basis_mod_MOD_binomial,"libfbasis.dll"), Int64, (Ptr{Int64},Ptr{Int64}), &n, &k)
    return binomial_result
end

binomial_result = binomial(6,3)
@show binomial_result


function bernstein_basis_real(a::Int64, p::Int64, t::Float64,dmin::Float64,dmax::Float64)
    #real(8) function bernstein_basis_real(a,p,t,dmin,dmax) result(ans)
    bernbasisreal_result = ccall( (:__basis_mod_MOD_bernstein_basis_real,"libfbasis.dll"),
                    Float64,
                    (Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
                    &a, &p, &t, &dmin, &dmax)
    return bernbasisreal_result
end

a = convert(Int64,1)
p = convert(Int64,3)
t = convert(Float64,-0.45)
dmin = convert(Float64,-1.0)
dmax = convert(Float64,1.0)
bernbasisreal_result = bernstein_basis_real(a,p,t,dmin,dmax)
@show bernbasisreal_result


function bezier_basis_real(i::Int64,order::Int64,kvlen::Int64,knot_v::Array{Float64,1},t::Float64)
    # recursive real(8) function bezier_basis_real(i,order,kvlen,knot_v,t) result(ans)
    bezbreal_result = ccall( (:__basis_mod_MOD_bezier_basis_real,"libfbasis.dll"),
                            Float64,
                            (Ptr{Int64}, Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}),
                            &i, &order, &kvlen, knot_v, &t)
    return bezbreal_result
end

i = convert(Int64,1)
order = convert(Int64,3)
knot_v = Array{Float64,1}([0.,0.,0.,0.,1.,1.,1.,1.])
kvlen = convert(Int64,length(knot_v))
t = convert(Float64,0.34)
bezbreal_result = bezier_basis_real(i,order,kvlen,knot_v,t)
@show bezbreal_result



# NOTE: So far, I have all the functions working for real numbers, but not yet for complex numbers.




function bernstein_basis_wrapper(a::Int64,p::Int64,t::Complex128,dmin::Float64,dmax::Float64)
    #subroutine bernstein_basis_wrapper(a, p, t_re, t_im, dmin, dmax, ans_reim)
    t_re = real(t)
    t_im = imag(t)
    ans_reim = zeros(Float64,2)
    ccall( (:__basis_mod_MOD_bernstein_basis_wrapper,"libfbasis.dll"),
                            Void,
                            (Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
                            &a, &p, &t_re, &t_im, &dmin, &dmax, ans_reim)
    return complex(ans_reim[1],ans_reim[2])
end

a = convert(Int64,1)
p = convert(Int64,3)
h = convert(Float64,1e-40)
t = convert(Complex128,-0.45+h*im)
dmin = convert(Float64,-1.0)
dmax = convert(Float64,1.0)
bernbasis_wrap_result = bernstein_basis_wrapper(a,p,t,dmin,dmax)
@show bernbasis_wrap_result
bernb_wrap_value = real(bernbasis_wrap_result)
bernb_wrap_deriv = imag(bernbasis_wrap_result)/h
@show bernb_wrap_value
@show bernb_wrap_deriv


function bezier_basis_wrapper(i::Int64,order::Int64,kvlen::Int64,knot_v::Array{Float64,1},t::Complex128)
    #subroutine bezier_basis_wrapper(i, order, kvlen, knot_v, t_re, t_im, ans_reim)
    t_re = real(t)
    t_im = imag(t)
    ans_reim = zeros(Float64,2)
    ccall( (:__basis_mod_MOD_bezier_basis_wrapper,"libfbasis.dll"),
                            Void,
                            (Ptr{Int64}, Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
                            &i, &order, &kvlen, knot_v, &t_re, &t_im, ans_reim)
    return complex(ans_reim[1],ans_reim[2])
end

i = convert(Int64,1)
order = convert(Int64,3)
knot_v = Array{Float64,1}([0.,0.,0.,0.,1.,1.,1.,1.])
kvlen = convert(Int64,length(knot_v))
h = convert(Float64,1e-40)
t = convert(Complex128,0.34+h*im)
bezb_wrap_result = bezier_basis_wrapper(i,order,kvlen,knot_v,t)
@show bezb_wrap_result
bezb_wrap_value = real(bezb_wrap_result)
bezb_wrap_deriv = imag(bezb_wrap_result)/h
@show bezb_wrap_value
@show bezb_wrap_deriv























# complex(16) function bernstein_basis(a,p,t,dmin,dmax)
#=
t = convert(Complex128,-0.45+0.0im)
dmin = convert(Float64,-1.0)
dmax = convert(Float64,1.0)
bernbasis_result = ccall( (:__basis_mod_MOD_bernstein_basis,"libfbasis.dll"),
                Complex{Float64},
                (Ptr{Int64}, Ptr{Int64}, Ptr{Complex{Float64}}, Ptr{Float64}, Ptr{Float64}),
                &1, &3, &t, &dmin, &dmax)
@show bernbasis_result
=#





#=
function bezier_basis_complex(i::Int64,order::Int64,kvlen::Int64,knot_v::Array{Float64,1},t::Complex128)
    # recursive complex(16) function bezier_basis(i,order,kvlen,knot_v,t) result(ans)
    bezb_result = ccall( (:__basis_mod_MOD_bezier_basis,"libfbasis.dll"),
                            Complex128,
                            (Ptr{Int64}, Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Complex128}),
                            &i, &order, &kvlen, knot_v, &t)
    return bezb_result
end

i = convert(Int64,1)
order = convert(Int64,3)
knot_v = Array{Float64,1}([0.,0.,0.,0.,1.,1.,1.,1.])
kvlen = convert(Int64,length(knot_v))
t = convert(Complex128,0.34+0.12345im)
bezb_result = bezier_basis_complex(i,order,kvlen,knot_v,t)
@show bezb_result
=#
