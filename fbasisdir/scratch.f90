! ----- bezier_basis -----
recursive real(8) function bezier_basis(i,order,knot_v,kvlen,t) result(ans)
    implicit none
    integer(8) :: i, order
    real(8), dimension(:) :: knot_v
    integer(8) :: kvlen
    real(8) :: t
    integer(8) :: p
    real(8) :: knot
    
    ans = 0.0d0
    p = order
    if (p >= 0) then
        if (p == 0) then
            if( (real(t) >= knot(knot_v,kvlen,i) ) .and. (real(t) < knot(knot_v,kvlen,i+1)) ) then
                ans = ans + 1.0d0
            endif
            return
        else
            if (knot(knot_v,kvlen,i+p) .ne. knot(knot_v,kvlen,i)) then
                ans = ans + ((t-knot(knot_v,kvlen,i))     / (knot(knot_v,kvlen,i+p)   &
                                - knot(knot_v,kvlen,i)  )) * bezier_basis(i,  p-1,knot_v,kvlen,t)
            endif
            if (knot(knot_v,i+p+1) .ne. knot(knot_v,i+1)) then
                ans = ans + ((knot(knot_v,kvlen,i+p+1)-t) / (knot(knot_v,kvlen,i+p+1) &
                                - knot(knot_v,kvlen,i+1))) * bezier_basis(i+1,p-1,knot_v,kvlen,t)
            endif
            return
        endif
    else
        return
    endif
    return
end function bezier_basis




___basis_helpers_mod_MOD_bezier_basis

___basis_helpers_mod_MOD_bernstein_basis


gfortran -shared -O2 fbasis.f90 -fPIC -o libfbasis.dll



    ! ----- bezier_basis_real -----
    real(8) function bezier_basis_real(i,order,kvlen,knot_v,t) result(ans)
        implicit none
        integer(8), intent(in) :: i, order
        integer(8), intent(in) :: kvlen
        real(8), dimension(kvlen), intent(in) :: knot_v
        real(8), intent(in) :: t
        ans = bezb_helper_real(i,order,kvlen,knot_v,t)
        return
    end function bezier_basis_real  