!----------------
! Steven Schmidt
! July, 2016
!----------------


! gfortran -shared -O2 fbasis.f90 -fPIC -o libfbasis.dll
! gfortran -o testout fbasis.f90 & ./testout


module basis_mod
    implicit none
    
    contains


    !**************************
    !**** HELPER FUNCTIONS ****
    !**************************

    ! ----- factorial -----
    ! n! = n*(n-1)*(n-2)*...*2*1
    integer(8) function factorial(a) result(ans)
        implicit none
        integer(8) :: a, b
        b = a
        ans = 1
        do while(b > 1)
            ans = ans * b
            b = b - 1
        end do
        return
    end function factorial


    ! ----- binomial -----
    ! n choose k
    ! n! / (k! * (n - k)!)
    integer(8) function binomial(n,k) result(ans)
        implicit none
        integer(8), intent(in) :: n, k
        ans = factorial(n) / factorial(k) / factorial(n-k)
        return
    end function binomial
    
    
    ! ----- knot_i -----
    integer(8) function knot_i(kvlen,i) result(ans)
        implicit none
        integer(8), intent(in) :: kvlen
        integer(8), intent(in) :: i
        if (i < 1) then
            ans = 1
        elseif(i > kvlen) then
            ans = kvlen
        else
            ans = i
        endif
        return
    end function knot_i







    !*******************************
    !**** REAL-VALUED FUNCTIONS ****
    !*******************************
    

    ! ----- bernstein_basis_real -----
    ! Parameters:
    !   a: The function index
    !   p: The polynomial order
    !   t: The parameter value where the basis function is to be evaluated
    !   dmin: The minimum value of the domain
    !   dmax: The maximum value of the domain
    real(8) function bernstein_basis_real(a,p,t,dmin,dmax) result(ans)
        implicit none
        integer(8), intent(in) :: a, p
        real(8), intent(in) :: t
        real(8), intent(in) :: dmin, dmax
        real(8) :: tmapped
        if (a < 1 .or. a > p+1) then
            ans = 0.0d0
            return
        endif
        tmapped = (t - dmin) / (dmax-dmin)
        ans = binomial(p,a-1) * tmapped**(a-1) * (1.0d0-tmapped)**(p-(a-1))
        return
    end function bernstein_basis_real



    ! ----- bezier_basis_real -----
    ! Parameters:
    !   i: The function index
    !   order: The polynomial order
    !   kvlen:  Length of the knot_v array
    !   knot_v: The knot vector array
    !   t: The parameter value where the basis function is to be evaluated
    recursive real(8) function bezier_basis_real(i,order,kvlen,knot_v,t) result(ans)
        implicit none
        integer(8), intent(in) :: i, order
        integer(8), intent(in) :: kvlen
        real(8), dimension(kvlen), intent(in) :: knot_v
        real(8), intent(in) :: t
        integer(8) :: p, j
        ans = 0.0d0
        p = order
        if (p >= 0) then
            if (p == 0) then
                if( (real(t) >= knot_v(knot_i(kvlen,i)) ) .and. (real(t) < knot_v(knot_i(kvlen,i+1)) ) ) then
                    ans = ans + 1.0d0
                endif
                return
            else
                if ( knot_v(knot_i(kvlen,i+p))   .ne. knot_v(knot_i(kvlen,i  )) ) then
                    ans = ans + (                                                     &
                          (t-knot_v(knot_i(kvlen,i    ))  )   /                       &
                          (knot_v(knot_i(kvlen,i+p  )) - knot_v(knot_i(kvlen,i  )) )  &
                          ) * bezier_basis_real(i,  p-1,kvlen,knot_v,t)
                endif
                if ( knot_v(knot_i(kvlen,i+p+1)) .ne. knot_v(knot_i(kvlen,i+1)) ) then
                    ans = ans + (                                                     &
                          (  knot_v(knot_i(kvlen,i+p+1))-t)   /                       &
                          (knot_v(knot_i(kvlen,i+p+1)) - knot_v(knot_i(kvlen,i+1)) )  &
                          ) * bezier_basis_real(i+1,p-1,kvlen,knot_v,t)
                endif
                return
            endif
        else
            return
        endif
        return
    end function bezier_basis_real







    !**********************************
    !**** COMPLEX-VALUED FUNCTIONS ****
    !**********************************


    ! ----- bernstein_basis -----
    ! Parameters:
    !   a: The function index
    !   p: The polynomial order
    !   t: The parameter value where the basis function is to be evaluated
    !   dmin: The minimum value of the domain
    !   dmax: The maximum value of the domain
    complex(16) function bernstein_basis(a,p,t,dmin,dmax) result(ans)
        implicit none
        integer(8), intent(in) :: a, p
        complex(16), intent(in) :: t
        real(8), intent(in) :: dmin, dmax
        complex(16) :: tmapped
!         write(*,*) "a = ",a
!         write(*,*) "p = ",p
!         write(*,*) "t = ",t
!         write(*,*) "dmin = ",dmin
!         write(*,*) "dmax = ",dmax
        if (a < 1 .or. a > p+1) then
            ans = cmplx(0.0d0,0.0d0,16)
            return
        endif
        tmapped = (t - dmin) / (dmax-dmin)
        ans = binomial(p,a-1) * tmapped**(a-1) * (1.0d0-tmapped)**(p-(a-1))
        return
    end function bernstein_basis

    ! ----- bernstein_basis_wrapper -----
    subroutine bernstein_basis_wrapper(a, p, t_re, t_im, dmin, dmax, ans_reim)
        implicit none
        integer(8), intent(in) :: a, p
        real(8), intent(in) :: t_re, t_im
        real(8), intent(in) :: dmin, dmax
        real(8), dimension(2), intent(out) :: ans_reim
        complex(16) :: t, cmpx_ans
        t = cmplx(t_re,t_im,16)
        cmpx_ans = bernstein_basis(a,p,t,dmin,dmax)
        ans_reim(1) = real(cmpx_ans)
        ans_reim(2) = imag(cmpx_ans)
        return
    end subroutine bernstein_basis_wrapper






    ! ----- bezier_basis -----
    ! Parameters:
    !   i: The function index
    !   order: The polynomial order
    !   kvlen:  Length of the knot_v array
    !   knot_v: The knot vector array
    !   t: The parameter value where the basis function is to be evaluated
    recursive complex(16) function bezier_basis(i,order,kvlen,knot_v,t) result(ans)
        implicit none
        integer(8), intent(in) :: i, order
        integer(8), intent(in) :: kvlen
        real(8), dimension(kvlen), intent(in) :: knot_v
        complex(16), intent(in) :: t
        integer(8) :: p, j
    
!        p = 4
!        write(*,*) "p = ",p
!        write(*,*) "got here"
!        write(*,*) "i = ",i
!        write(*,*) "order = ",order
!        write(*,*) "kvlen = ",kvlen
!        do j = 1,8
!            write(*,*) "j,knot_v(j): ",j,knot_v(j)
!        end do
!         ans = cmplx(3.0d0,1.4d0,16)
!         write(*,*) ans
!         return
    
        ans = cmplx(0.0d0,0.0d0)
        p = order
        if (p >= 0) then
            if (p == 0) then
                if( (real(t) >= knot_v(knot_i(kvlen,i)) ) .and. (real(t) < knot_v(knot_i(kvlen,i+1)) ) ) then
                    ans = ans + cmplx(1.0d0,0.0d0)
                endif
                return
            else
                if ( knot_v(knot_i(kvlen,i+p))   .ne. knot_v(knot_i(kvlen,i  )) ) then
                    ans = ans + (                                                     &
                          (t-knot_v(knot_i(kvlen,i    ))  )   /                       &
                          (knot_v(knot_i(kvlen,i+p  )) - knot_v(knot_i(kvlen,i  )) )  &
                          ) * bezier_basis(i,  p-1,kvlen,knot_v,t)
                endif
                if ( knot_v(knot_i(kvlen,i+p+1)) .ne. knot_v(knot_i(kvlen,i+1)) ) then
                    ans = ans + (                                                     &
                          (  knot_v(knot_i(kvlen,i+p+1))-t)   /                       &
                          (knot_v(knot_i(kvlen,i+p+1)) - knot_v(knot_i(kvlen,i+1)) )  &
                          ) * bezier_basis(i+1,p-1,kvlen,knot_v,t)
                endif
                return
            endif
        else
            return
        endif
        return
    end function bezier_basis

    ! ----- bezier_basis_wrapper -----
    subroutine bezier_basis_wrapper(i, order, kvlen, knot_v, t_re, t_im, ans_reim)
        implicit none
        integer(8), intent(in) :: i, order
        integer(8), intent(in) :: kvlen
        real(8), dimension(kvlen), intent(in) :: knot_v
        real(8), intent(in) :: t_re, t_im
        real(8), dimension(2), intent(out) :: ans_reim
        complex(16) :: t, cmpx_ans
        integer(8) :: p, j
        t = cmplx(t_re,t_im,16)
        cmpx_ans = bezier_basis(i,order,kvlen,knot_v,t)
        ans_reim(1) = real(cmpx_ans)
        ans_reim(2) = imag(cmpx_ans)
        return
    end subroutine bezier_basis_wrapper


end module basis_mod





! ----- testfbasis -----
! program testfbasis
!     use basis_mod
!     implicit none
!     complex(16) :: val
!     real(8) :: t, f, dfdx, h
!     complex(16) :: t_cmplx
!     integer(8) :: a, p
!     real(8) :: dmin, dmax  
!     integer(8) :: kvlen, k
!     real(8), dimension(8) :: knot_v
!     integer(8) :: j, k_val
!     integer(8) :: i, order
!      
! 
!     ! Testing "bernstein_basis" function 
!     write(*,*) "Testing bernstein_basis function"
!     a = 1
!     p = 3
!     t = -0.45d0
!     dmin = -1.0d0
!     dmax = 1.0d0
!     h = 1.0d-40
!     t_cmplx = cmplx(t,0.0d0,16)
!     f = real(bernstein_basis(a,p,t_cmplx,dmin,dmax))
!     t_cmplx = cmplx(t,h,16)
!     dfdx = imag(bernstein_basis(a,p,t_cmplx,dmin,dmax)) / h
!     write(*,*) "f = ", f, "dfdx = ",dfdx
!     
!     
!     ! Testing "knot_i" function
!     write(*,*) "Testing knot_i function"
!     do j = 1,4
!         knot_v(j) = 0.0
!     enddo
!     do j = 5,8
!         knot_v(j) = 1.0
!     enddo
!     kvlen = 8
!     k = 5
!     k_val = knot_v(knot_i(kvlen,k))
!     write(*,*) "k_val:",k_val
!     
!     
!     ! Testing "bezier_basis" function
!     write(*,*) "Testing bezier_basis function"
!     i = 1
!     order = 3
!     t = 0.34d0
!     h = 1.0d-40
!     t_cmplx = cmplx(t,0.0d0,16)
!     f = real(bezier_basis(i,order,kvlen,knot_v,t_cmplx))
!     t_cmplx = cmplx(t,h,16)
!     dfdx = imag(bezier_basis(i,order,kvlen,knot_v,t_cmplx)) / h
!     write(*,*) "f = ", f, "dfdx = ",dfdx
! 
! end program testfbasis







