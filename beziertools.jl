module SSBezierTools

# BezierTools
# Tools for computing bezier, b-spline, and nurbs curves and basis functions
# Steven Schmidt, 2016

# TODO: Convert documentation to Julia-compliant doc syntax.
# http://docs.julialang.org/en/release-0.4/manual/documentation/?highlight=%40#@__doc__


function __init__()
    global const bernbasis_fptr = cglobal(
        (:__basis_mod_MOD_bernstein_basis_wrapper,"ssbeziertoolsjl/fbasisdir/libfbasis.dll") )
    global const bezbasis_fptr = cglobal(
        (:__basis_mod_MOD_bezier_basis_wrapper,"ssbeziertoolsjl/fbasisdir/libfbasis.dll") )
end



"""
function BernsteinBasis(a::Int64,
                        p::Int64,
                        t::Float64;
                        dmin::Float64=0.,
                        dmax::Float64=1.)
============================
# Parameters
* a: The function index
* p: The polynomial order
* t: The parameter value where the basis function is to be evaluated

# Named Parameters
* dmin: The minimum value of the domain
* dmax: The maximum value of the domain
"""
function BernsteinBasis(a::Int64,
                        p::Int64,
                        t::Complex{Float64};
                        dmin::Float64=0.,
                        dmax::Float64=1.)

    if(false)
        # Use Fortran code
        t_re = real(t)
        t_im = imag(t)
        ans_reim = zeros(Float64,2)
        #ccall( (:__basis_mod_MOD_bernstein_basis_wrapper,"ssbeziertoolsjl/fbasisdir/libfbasis.dll"),
        ccall( bernbasis_fptr,
                                Void,
                                (Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
                                &a, &p, &t_re, &t_im, &dmin, &dmax, ans_reim)
        return complex(ans_reim[1],ans_reim[2])

    else
        # Use Julia code

        if(a < 1 || a > p+1)
            return 0.0
        end
        tmapped = (t - dmin)/(dmax-dmin)
        result = binomial(p,a-1) * tmapped^(a-1) * (1.-tmapped)^(p-(a-1))
        return result
    end


end
function BernsteinBasis(a::Int64,
                        p::Int64,
                        t::Float64;
                        dmin::Float64=0.,
                        dmax::Float64=1.)
    return real(BernsteinBasis(a,p,complex(t),dmin=dmin,dmax=dmax))
end
function BernsteinBasis(a::Int64,
                        p::Int64,
                        t::Union{Array{Float64},FloatRange{Float64}};
                        dmin::Float64=0.,
                        dmax::Float64=1.)
    len_t = length(t)
    return [BernsteinBasis(a,p,complex(t[i]),dmin=dmin,dmax=dmax) for i=1:len_t]
end


"""
function DelBernsteinBasis(a::Int64,
                           p::Int64,
                           t::Float64;
                           dmin::Float64=0.,
                           dmax::Float64=1.)
===============

# Parameters
* a: The function index
* p: The polynomial order
* t: The parameter value where the basis function is to be evaluated

# Named Parameters
* dmin: The minimum value of the domain
* dmax: The maximum value of the domain
"""
function DelBernsteinBasis(a::Int64,
                           p::Int64,
                           t::Float64;
                           dmin::Float64=0.,
                           dmax::Float64=1.)
    #=
    tmapped = (t - dmin)/(dmax-dmin)
    result = p * (BernsteinBasis(a-1,p-1,tmapped) - BernsteinBasis(a,p-1,tmapped)) / (dmax-dmin)
    return result
    =#
    tmapped = (t - dmin)/(dmax-dmin)
    h = 1e-40
    return (imag(BernsteinBasis(a,p,tmapped+h*im))/h) / (dmax-dmin)
end
function DelBernsteinBasis(a::Int64,
                           p::Int64,
                           t::Union{Array{Float64},FloatRange{Float64}};
                           dmin::Float64=0.,
                           dmax::Float64=1.)
    len_t = length(t)
    return Array{Float64}([DelBernsteinBasis(a,p,t[i],dmin=dmin,dmax=dmax) for i=1:len_t])
end






"""
function UniqueKnotCount(knot_v::Array{Float64})
=================
# Parameters:
* knot_v: Valid knot vector, ::Array{Float64}

# Returns:
* Number of unique values in the knot vector
"""
function UniqueKnotCount(knot_v::Array{Float64})
    val = -1
    count = 0
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] != val)
            val = knot_v[i]
            count += 1
        end
    end
    return count
end

"""
function UniqueKnotArray(knot_v::Array{Float64})
=================
# Parameters:
* knot_v: Valid knot vector, ::Array{Float64}

# Returns:
* Array containing each unique knot value (once each)
"""
function UniqueKnotArray(knot_v::Array{Float64})

    # Determine how many unique values there are.
    count = UniqueKnotCount(knot_v)

    # Init the array
    uniqueknots = zeros(count)

    # Fill the array with knots
    val = -1
    j = 1
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] != val)
            val = knot_v[i]
            uniqueknots[j] = knot_v[i]
            j += 1
        end
    end

    return uniqueknots
end



"""
function Multiplicity(knot_v::Array{Float64},knot::Float64)
=================

Returns the multiplicity of a knot in a knot vector.

# Parameters:
* knot_v: The knot vector, ::Array{Float64}
* knot: The knot we're asking about, ::Float64

# Returns:
* The multiplicity of the knot.
"""
function Multiplicity(knot_v::Array{Float64},knot::Float64)
    count = 0
    len_kv = length(knot_v)
    for i = 1:len_kv
        if(knot_v[i] > knot)
            break
        end
        if(knot_v[i] == knot)
            count += 1
        end
    end
    return count
end









"""
function Knot(knot_v::Array{Float64},i::Int64)
=======
Returns the knot at index i, or
if i < 1, then returns knot_v[1]
if i > length(knot_v), then returns knot_v[end]

# Parameters:
* knot_v: The knot vector
* i: The desired index
"""
function Knot(knot_v::Array{Float64},i::Int64)
    if(i < 1)
        return knot_v[1]
    elseif(i > length(knot_v))
        return knot_v[end]
    end
    return knot_v[i]
end

"""
function bezier_basis_helper(i::Int64,order::Int64,
                             knot_v::Array{Float64},t::Float64)
=========

# Parameters:
* i: knot index
* order: The order of the curve
* knot_v: The knot vector. Example: numpy.array([0,0,0,1,2,3,3,3])
* t: The parameter value where the basis function is evaluated
    (cannot be array; must be single value)
"""
function bezier_basis_helper(i::Int64,order::Int64,
                             knot_v::Array{Float64},t::Complex{Float64})
    if(false)
        # Fortran implemented function
        t_re = real(t)
        t_im = imag(t)
        ans_reim = zeros(Float64,2)
        kvlen = convert(Int64,length(knot_v))
        #ccall( (:__basis_mod_MOD_bezier_basis_wrapper,"ssbeziertoolsjl/fbasisdir/libfbasis.dll"),
        ccall( bezbasis_fptr,
                                Void,
                                (Ptr{Int64}, Ptr{Int64}, Ptr{Int64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}, Ptr{Float64}),
                                &i, &order, &kvlen, knot_v, &t_re, &t_im, ans_reim)
        return complex(ans_reim[1],ans_reim[2])

    else
        # Julia implemented function
        ans = 0.0
        p = order
        if (p >= 0)
            if (p == 0)
                ans += convert( Int64,(real(t) >= Knot(knot_v,i) ) && (real(t) < Knot(knot_v,i+1)))
                return ans
            else
                if (Knot(knot_v,i+p) != Knot(knot_v,i))
                    ans += ((t - Knot(knot_v,i)) /
                            (Knot(knot_v,i+p)   - Knot(knot_v,i)  )) *
                            bezier_basis_helper(i  , p-1, knot_v, t)
                end

                if (Knot(knot_v,i+p+1) != Knot(knot_v,i+1))
                    ans += ((Knot(knot_v,i+p+1) - t) /
                            (Knot(knot_v,i+p+1) - Knot(knot_v,i+1))) *
                            bezier_basis_helper(i+1, p-1, knot_v, t)
                end

                return ans
            end
        else
            return ans
        end

    end
end
function bezier_basis_helper(i::Int64,order::Int64,
                             knot_v::Array{Float64},t::Float64)
    return real(bezier_basis_helper(i,order,knot_v,complex(t)))
end

"""
function BezierBasis(i::Int64,order::Int64,
                     knot_v::Array{Float64},
                     t::Union{Array{Float64},FloatRange{Float64}})
=============

# Parameters:
* i: knot index
* order: The order of the curve
* knot_v: The knot vector. Example: [0.,0,0,1,2,3,3,3]
* t: Array of floats: The parameter values where the basis function
      is evaluated
* Returns: The value of the Bezier Basis evaluated at values t.
"""
function BezierBasis(i::Int64,
                     order::Int64,
                     knot_v::Array{Float64},
                     t::Float64)
    return bezier_basis_helper(i,order,knot_v,t)
end
function BezierBasis(i::Int64,
                     order::Int64,
                     knot_v::Array{Float64},
                     t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    ans = [bezier_basis_helper(i,order,knot_v,t[j]) for j=1:len_t]
    return ans
end
function BezierBasis(i::Array{Int64},
                     order::Array{Int64},
                     knot_v::Array{Array{Float64}},
                     t::Array{Float64})
    # Multi-d case
    val = 1.0
    for k = 1:length(i)
        val *= bezier_basis_helper(i[k],order[k],knot_v[k],t[k])
    end
    return val
end







"""
function del_bezier_basis_helper(i::Int64,order::Int64,
                                 knot_v::Array{Float64},t::Float64)
============
Helper function for DelBezierBasis

# Parameters
* i: knot index
* order: The order of the curve
* knot_v: The knot vector. Example: [0.,0,0,1,2,3,3,3]
* t: The parameter value where the basis function is evaluated
     (cannot be array; must be single value)
"""
function del_bezier_basis_helper(i::Int64,order::Int64,
                                 knot_v::Array{Float64},t::Float64)
    #=
    ans = 0
    p = order
    if (p >= 1)
        if ( Knot(knot_v,i+p)   != Knot(knot_v,i) )
            ans += (p / (Knot(knot_v,i+p) - Knot(knot_v,i))) *
                    bezier_basis_helper(i, p-1, knot_v, t)
        end
        if ( Knot(knot_v,i+p+1) != Knot(knot_v,i+1) )
            ans += (-1.) * (p / (Knot(knot_v,i+p+1) - Knot(knot_v,i+1))) *
                    bezier_basis_helper(i+1, p-1, knot_v, t)
        end
        return ans
    end
    return ans
    =#

    #=
    h = 1e-40
    val = bezier_basis_helper(i,order,knot_v,t+h*im)
    return real(val), imag(val)/h
    =#

    h = 1e-40
    return imag(bezier_basis_helper(i,order,knot_v,t+h*im))/h
end




"""
function DelBezierBasis(i::Int64,order::Int64,
                        knot_v::Array{Float64},
                        t::Union{Array{Float64},FloatRange{Float64}})
========

# Parameters
* i: knot index
* order: The order of the curve
* knot_v: he knot vector. Example: Array[[0,0,0,1,2,3,3,3]]
* t: Array of floats: The parameter values where the del_basis function is evaluated
* Returns: The value of the derivative of the Bezier Basis evaluated at values t.
"""
function DelBezierBasis(i::Int64,order::Int64,
                        knot_v::Array{Float64},
                        t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    ans = [del_bezier_basis_helper(i,order,knot_v,t[j]) for j=1:len_t]
    return ans
end
function DelBezierBasis(i::Int64,order::Int64,
                        knot_v::Array{Float64},t::Float64)
    return del_bezier_basis_helper(i,order,knot_v,t)
end





"""
function weights_sum_term(order::Int64,knot_v::Array{Float64},
                          weights::Array{Float64},t::Float64)
=======
Helper function for NurbsBasis

# Parameters:
* order: The order of the curve
* knot_v: The knot vector
* weights: The weights of the nurbs spline
* t: must be a single value (not an array)
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)

# Returns:
* Sum[ weight_i * bez_basis_i, {i} ]

"""
function weights_sum_term(order::Int64,knot_v::Array{Float64},
                          weights::Array{Float64},t::Float64)
    W = 0.
    val = 0.
    len_w = length(weights)
    for j = 1:len_w
        val = bezier_basis_helper(j,order,knot_v,t) * weights[j]
        W += val
    end
    return W
end
function weights_sum_term(order::Int64,knot_v::Array{Float64},
                          weights::Array{Float64},t::Float64,
                          cpsupport::Array{Int64})
    W = 0.
    val = 0.
    for j in cpsupport
        val = bezier_basis_helper(j,order,knot_v,t) * weights[j]
        W += val
    end
    return W
end

"""
function nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                            weights::Array{Float64},t::Float64)
============
Helper function for NurbsBasis

* Note: 't' must be a single value (not an array)
"""
function nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                            weights::Array{Float64},t::Float64)
    numerator =  bezier_basis_helper(i,order,knot_v,t) * weights[i]
    denominator = weights_sum_term(order,knot_v,weights,t)
    result = numerator/denominator
    if(isnan(result))
        result = 0.0
    end
    return result
end
function nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                            weights::Array{Float64},t::Float64,
                            cpsupport::Array{Int64})
    numerator =  bezier_basis_helper(i,order,knot_v,t) * weights[i]
    denominator = weights_sum_term(order,knot_v,weights,t,cpsupport)
    result = numerator/denominator
    if(isnan(result))
        result = 0.0
    end
    return result
end


"""
function NurbsBasis(i::Int64,order::Int64,knot_v::Array{Float64},
                    weights::Array{Float64},
                    t::Union{Array{Float64},FloatRange{Float64}})
===========

# Parameters:
* i: knot index
* order: The order of the curve
* knot_v: The knot vector
* weights: The weights of the nurbs spline
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the NURBS basis evaluated at values t.
"""
function NurbsBasis(i::Int64,
                    order::Int64,
                    knot_v::Array{Float64},
                    weights::Array{Float64},
                    t::Float64)
    return nurbs_basis_helper(i,order,knot_v,weights,t)
end
function NurbsBasis(i::Int64,
                    order::Int64,
                    knot_v::Array{Float64},
                    weights::Array{Float64},
                    t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    ans = [nurbs_basis_helper(i,order,knot_v,weights,t[j]) for j=1:len_t]
    return ans
end
function NurbsBasis(i::Array{Int64},
                    order::Array{Int64},
                    knot_v::Array{Array{}},
                    weights::Array{Array{}},
                    t::Array{Float64})
    # Multi-d case
    # NOTE: In this function, "t" is a single point in multi-d space
    val = 1.0
    for k = 1:length(i)
        val *= nurbs_basis_helper(i[k],order[k],knot_v[k],weights[k],t[k])
    end
    return val
end








"""
function del_weights_sum_term(order::Int64,knot_v::Array{Float64},
                              weights::Array{Float64},t::Float64)
=========
Helper function for DelNurbsBasis

# Parameters:
* order: The order of the curve
* knot_v: The knot vector
* weights: The weights of the nurbs spline
* t: must be a single value (not an array)
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)
"""
function del_weights_sum_term(order::Int64,knot_v::Array{Float64},
                              weights::Array{Float64},t::Float64)
    delW = 0.
    val = 0.
    len_w = length(weights)
    for j = 1:len_w
        val = del_bezier_basis_helper(j,order,knot_v,t) * weights[j]
        delW += val
    end
    return delW
end
function del_weights_sum_term(order::Int64,knot_v::Array{Float64},
                              weights::Array{Float64},t::Float64,
                              cpsupport::Array{Int64})
    delW = 0.
    val = 0.
    for j in cpsupport
        val = del_bezier_basis_helper(j,order,knot_v,t) * weights[j]
        delW += val
    end
    return delW
end



"""
function del_nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                                weights::Array{Float64},t::Float64)
=========
Helper function for DelNurbsBasis

* Note: 't' must be a single value (not an array)

* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)
"""
function del_nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                                weights::Array{Float64},t::Float64)
    #Defined on pages 51-52 of IGA book.
    W = weights_sum_term(order,knot_v,weights,t)
    delW = del_weights_sum_term(order,knot_v,weights,t)

    delBezN = del_bezier_basis_helper(i,order,knot_v,t)
    bezN = bezier_basis_helper(i,order,knot_v,t)

    result = weights[i] * ((W*delBezN - delW*bezN) / W^2)
    return result
end
function del_nurbs_basis_helper(i::Int64,order::Int64,knot_v::Array{Float64},
                                weights::Array{Float64},t::Float64,
                                cpsupport::Array{Int64})
    #Defined on pages 51-52 of IGA book.
    W = weights_sum_term(order,knot_v,weights,t,cpsupport)
    delW = del_weights_sum_term(order,knot_v,weights,t,cpsupport)

    delBezN = del_bezier_basis_helper(i,order,knot_v,t)
    bezN = bezier_basis_helper(i,order,knot_v,t)

    result = weights[i] * ((W*delBezN - delW*bezN) / W^2)
    return result
end




"""
function DelNurbsBasis(i::Int64,
                       order::Int64,
                       knot_v::Array{Float64},
                       weights::Array{Float64},
                       t::Union{Array{Float64},FloatRange{Float64}})
=========

# Parameters:
* i: knot index
* order: The order of the curve
* knot_v: The knot vector
* weights: The weights of the nurbs spline
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the derivative of the NURBS basis evaluated at values t.
"""
function DelNurbsBasis(i::Int64,
                       order::Int64,
                       knot_v::Array{Float64},
                       weights::Array{Float64},
                       t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    ans = [del_nurbs_basis_helper(i,order,knot_v,weights,t[j]) for j=1:len_t]
    return ans
end
function DelNurbsBasis(i::Int64,
                       order::Int64,
                       knot_v::Array{Float64},
                       weights::Array{Float64},
                       t::Float64)
    return del_nurbs_basis_helper(i,order,knot_v,weights,t)
end









"""
function NurbsDomainX(order::Int64,
                        knot_v::Array{Float64},
                        control_pts::Array{Float64},
                        weights::Array{Float64},
                        t::Float64)
======

# Notes:
* control_pts: Must be a 1d array, with only one dimension of the control points.
* t: Must be a single value (not an array)

# Parameters:
* order
* knot_v
* control_pts
* weights
* t
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)

# Returns:
* The x values corresponding to parameter values t.
"""
function NurbsDomainX(order::Int64,
                        knot_v::Array{Float64},
                        control_pts::Array{Float64},
                        weights::Array{Float64},
                        t::Float64)
    X = 0.
    ncp = length(control_pts)
    for i = 1:ncp
        X += nurbs_basis_helper(i,order,knot_v,weights,t) * control_pts[i]
    end
    return X
end
function NurbsDomainX(order::Int64,
                        knot_v::Array{Float64},
                        control_pts::Array{Float64},
                        weights::Array{Float64},
                        t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    return [NurbsDomainX(order,knot_v,
                           control_pts,weights,t[i]) for i = 1:len_t]
end
function NurbsDomainX(order::Int64,
                        knot_v::Array{Float64},
                        control_pts::Array{Float64},
                        weights::Array{Float64},
                        t::Float64,
                        cpsupport::Array{Int64})
    X = 0.
    ncp = length(control_pts)
    for i in cpsupport
        X += nurbs_basis_helper(i,order,knot_v,weights,t,cpsupport) * control_pts[i]
    end
    return X
end
function NurbsDomainX(order::Int64,
                        knot_v::Array{Float64},
                        control_pts::Array{Float64},
                        weights::Array{Float64},
                        t::Union{Array{Float64},FloatRange{Float64}},
                        cpsupport::Array{Int64})
    len_t = length(t)
    return [NurbsDomainX(order,knot_v,
                           control_pts,weights,t[i],
                           cpsupport) for i = 1:len_t]
end
function NurbsDomainX(order::Array{Int64},
                        knot_v::Array{Array{}},
                        control_pts::Array{Float64},
                        weights::Array{Array{}},
                        t::Union{Float64,Array{Float64}},
                        cpsupport::Array{Array{}})

    # Multi-d version.  "cpsuport" version
    # This is what we're doing.  This function returns the spacial domain X point
    # for a parameter point t, in 1D, 2D, and 3D.  Initially, we need to get this
    # function working to the point where it can draw a dim_s=2,dim_p=1 curve, and when
    # that works it'll be easier to see how to make it work in arbitrary dimensions.
    # See page 51 of the IGA book.

    # Multi-d
    dim_p = length(t) # "t" is just a single point in parameter space.
    dim_s = nothing
    if(dim_p == 1)
        dim_s = length(control_pts[:,1])
    elseif(dim_p == 2)
        dim_s = length(control_pts[:,1,1])
    elseif(dim_p == 3)
        dim_s = length(control_pts[:,1,1,1])
    end

    X = zeros(dim_s,1)

    if(dim_p == 1)
        for i_d1 in cpsupport[1]
            NBH_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
            X += (NBH_d1 * control_pts[:,i_d1])[:]
        end

    elseif(dim_p == 2)
        for i_d1 in cpsupport[1]
            NBH_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
            for i_d2 in cpsupport[2]
                NBH_d2 = nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2],cpsupport[2])
                X += (NBH_d1 * NBH_d2 * control_pts[:,i_d1,i_d2])[:]
            end
        end

    elseif(dim_p == 3)
        for i_d1 in cpsupport[1]
            NBH_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
            for i_d2 in cpsupport[2]
                NBH_d2 = nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2],cpsupport[2])
                for i_d3 in cpsupport[3]
                    NBH_d3 = nurbs_basis_helper(i_d3,order[3],knot_v[3],weights[3],t[3],cpsupport[3])
                    X += (NBH_d1 * NBH_d2 * NBH_d3 * control_pts[:,i_d1,i_d2,i_d3])[:]
                end
            end
        end

    end

    return X
end
function NurbsDomainX(order::Array{Int64},
                        knot_v::Array{Array{}},
                        control_pts::Array{Float64},
                        weights::Array{Array{}},
                        t::Union{Float64,Array{Float64}})
    dim_p = length(t)
    cpsupport = nothing
    if(dim_p == 1)
        ncp_d1 = size(control_pts)[2]
        cpsupport = Array[collect(1:ncp_d1)]
    elseif(dim_p == 2)
        ncp_d1 = size(control_pts)[2]
        ncp_d2 = size(control_pts)[3]
        cpsupport = Array[collect(1:ncp_d1),collect(1:ncp_d2)]
    elseif(dim_p == 3)
        ncp_d1 = size(control_pts)[2]
        ncp_d2 = size(control_pts)[3]
        ncp_d3 = size(control_pts)[4]
        cpsupport = Array[collect(1:ncp_d1),collect(1:ncp_d2),collect(1:ncp_d3)]
    end
    return NurbsDomainX(order,knot_v,control_pts,weights,t,cpsupport)
end



"""
function NurbsDomainDXDt(order::Int64,
                           knot_v::Array{Float64},
                           control_pts::Array{Float64},
                           weights::Array{Float64},
                           t::Float64)
======

# Notes:
* t: must be a single value (not an array)
* control_pts: must be a 1d array, with only one dimension of the control points.

# Parameters:
* order
* knot_v
* control_pts
* weights
* t
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)

# Returns:
* The dx/dt values corresponding to parameter values t.
"""
function NurbsDomainDXDt(order::Int64,
                           knot_v::Array{Float64},
                           control_pts::Array{Float64},
                           weights::Array{Float64},
                           t::Float64)
    dXdt = 0.
    ncp = length(control_pts)
    for i = 1:ncp
        dXdt += del_nurbs_basis_helper(i,order,knot_v,weights,t) * control_pts[i]
    end
    return dXdt
end
function NurbsDomainDXDt(order::Int64,
                           knot_v::Array{Float64},
                           control_pts::Array{Float64},
                           weights::Array{Float64},
                           t::Union{Array{Float64},FloatRange{Float64}})
    len_t = length(t)
    return [NurbsDomainDXDt(order,knot_v,
                              control_pts,weights,t[i]) for i = 1:len_t]
end
function NurbsDomainDXDt(order::Int64,
                           knot_v::Array{Float64},
                           control_pts::Array{Float64},
                           weights::Array{Float64},
                           t::Float64,
                           cpsupport::Array{Int64})
    dXdt = 0.
    #ncp = length(control_pts)
    for i in cpsupport
        dXdt += del_nurbs_basis_helper(i,order,knot_v,
                                weights,t,cpsupport) * control_pts[i]
    end
    return dXdt
end
function NurbsDomainDXDt(order::Int64,
                           knot_v::Array{Float64},
                           control_pts::Array{Float64},
                           weights::Array{Float64},
                           t::Union{Array{Float64},FloatRange{Float64}},
                           cpsupport::Array{Int64})
    len_t = length(t)
    return [NurbsDomainDXDt(order,knot_v,
                              control_pts,weights,t[i],cpsupport) for i = 1:len_t]
end
function NurbsDomainDXDt(order::Array{Int64},
                         knot_v::Array{Array{}},
                         control_pts::Array{Float64},
                         weights::Array{Array{}},
                         t::Union{Float64,Array{Float64}},
                         cpsupport::Array{Array{}})

    # Multi-d version; "cpsupport" version
    dim_p = length(t) # "t" is just a single point in parameter space.
    dim_s = nothing
    if(dim_p == 1)
        dim_s = length(control_pts[:,1])
    elseif(dim_p == 2)
        dim_s = length(control_pts[:,1,1])
    elseif(dim_p == 3)
        dim_s = length(control_pts[:,1,1,1])
    end

    DXDT = zeros(dim_s,dim_p)

    if(dim_p == 1)
        for pdir = 1:dim_p
            for i_d1 in cpsupport[1]
                if( pdir == 1 )
                    nbh_d1 = del_nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
                else
                    nbh_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1])
                end
                DXDT[:,pdir] += (nbh_d1 * control_pts[:,i_d1])[:]
            end
        end

    elseif(dim_p == 2)
        for pdir = 1:dim_p
            for i_d1 in cpsupport[1]
                if( pdir == 1 )
                    nbh_d1 = del_nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
                else
                    nbh_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1])
                end
                for i_d2 in cpsupport[2]
                    if( pdir == 2 )
                        nbh_d2 = del_nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2],cpsupport[2])
                    else
                        nbh_d2 = nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2])
                    end

                    DXDT[:,pdir] += (nbh_d1 * nbh_d2 * control_pts[:,i_d1,i_d2])[:]
                end
            end
        end


    elseif(dim_p == 3)
        for pdir = 1:dim_p
            for i_d1 in cpsupport[1]
                if( pdir == 1 )
                    nbh_d1 = del_nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1],cpsupport[1])
                else
                    nbh_d1 = nurbs_basis_helper(i_d1,order[1],knot_v[1],weights[1],t[1])
                end
                for i_d2 in cpsupport[2]
                    if( pdir == 2 )
                        nbh_d2 = del_nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2],cpsupport[2])
                    else
                        nbh_d2 = nurbs_basis_helper(i_d2,order[2],knot_v[2],weights[2],t[2])
                    end
                    for i_d3 in cpsupport[3]
                        if( pdir == 3 )
                            nbh_d3 = del_nurbs_basis_helper(i_d3,order[3],knot_v[3],weights[3],t[3],cpsupport[3])
                        else
                            nbh_d3 = nurbs_basis_helper(i_d3,order[3],knot_v[3],weights[3],t[3])
                        end
                        DXDT[:,pdir] += (nbh_d1 * nbh_d2 * nbh_d3 * control_pts[:,i_d1,i_d2,i_d3])[:]
                    end
                end
            end
        end
    end

    # Returns: Matrix of dimension dim_s-by-dim_p
    return DXDT
end
function NurbsDomainDXDt(order::Array{Int64},
                        knot_v::Array{Array{}},
                        control_pts::Array{Float64},
                        weights::Array{Array{}},
                        t::Union{Float64,Array{Float64}})
    dim_p = length(t)
    cpsupport = nothing
    if(dim_p == 1)
        ncp_d1 = size(control_pts)[2]
        cpsupport = Array[collect(1:ncp_d1)]
    elseif(dim_p == 2)
        ncp_d1 = size(control_pts)[2]
        ncp_d2 = size(control_pts)[3]
        cpsupport = Array[collect(1:ncp_d1),collect(1:ncp_d2)]
    elseif(dim_p == 3)
        ncp_d1 = size(control_pts)[2]
        ncp_d2 = size(control_pts)[3]
        ncp_d3 = size(control_pts)[4]
        cpsupport = Array[collect(1:ncp_d1),collect(1:ncp_d2),collect(1:ncp_d3)]
    end
    return NurbsDomainDXDt(order,knot_v,control_pts,weights,t,cpsupport)
end




"""
function NurbsDraw(dim_p::Int64,
                   dim_s::Int64,
                   order::Int64,
                   knot_v::Array{Array{}},
                   spans::Array{Int64},
                   control_pts::Array{Float64,2},
                   weights::Array{Float64},
                   res::Array{Int64})
=====

# Parameters:
* dim_p: Parameter-space dimensions
* dim_s: Spatial dimensions
* order: The polynomial order of the curve in each parameter-space direction.
         Array of integers. Example: [3,2]
* knot_v: The array of knot vectors, for each spatial direction.
            Surface example: Array[[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]]
* spans:  The number of spans between control points in each direction
          Array of integers. Example: [5,6]
* control_pts:  A matrix of dimension (dim_s-by-n) for n control pts, containing the
                spatial coordinates of each control point.
* weights: An array of length n, for n control points - one weight for each control point.
* res: The number of values in the array t, in each direction
       Example:  if paramter t goes from 0 to 2, and res = 4, then t becomes 0,0.5,1,1.5

# Returns:
* X,t,h: X is x values of curve; t is t values, h is interval in t.
"""
function NurbsDraw(dim_p::Int64,
                   dim_s::Int64,
                   order::Array{Int64},
                   knot_v::Array{Array{}},
                   spans::Array{Int64},
                   control_pts::Array{Float64},
                   weights::Array{Array{}},
                   res::Array{Int64})

    if(dim_p == 1)
        h = (maximum(knot_v[1]) - minimum(knot_v[1]))/res[1]
        t = collect( minimum(knot_v[1]):h:maximum(knot_v[1])-h )
        t = reshape(t,1,res[1])
        X = zeros(dim_s,res[1])
        for i = 1:res[1]
            X[:,i] = NurbsDomainX(order,knot_v,control_pts,weights,t[i])
        end
        return X,t,[h]
    end
    if(dim_p == 2)
        h1 = (maximum(knot_v[1]) - minimum(knot_v[1]))/res[1]
        h2 = (maximum(knot_v[2]) - minimum(knot_v[2]))/res[2]
        t1 = 0:h1:(1-h1)
        t2 = 0:h2:(1-h2)
        T1 = zeros(res[1],res[2])
        T2 = zeros(res[1],res[2])
        T1[:,:] = [t1[i] for i = 1:res[1], j = 1:res[2]]
        T2[:,:] = [t2[j] for i = 1:res[1], j = 1:res[2]]
        T = zeros(2,res[1]*res[2])
        for y_i = 1:res[2]
            for x_i = 1:res[1]
                T[1,(y_i-1)*res[1]+x_i] = T1[x_i,y_i]
                T[2,(y_i-1)*res[1]+x_i] = T2[x_i,y_i]
            end
        end
        X = zeros(dim_s,res[1]*res[2])
        for i = 1:res[1]*res[2]
            X[:,i] = NurbsDomainX(order,knot_v,control_pts,weights,T[:,i])
        end
        return X,T,[h1,h2]
    end

end






"""
function BezExtOpMatrix(new_knot::Float64,
                        order::Int64,
                        knot_v::Array{Float64})
======
This function is for B-splines.  Returns the bezier extraction
operator for inserting a knot into the knot_vector

# Parameters:
* new_knot: The new knot to be inserted
* order: The order of the curve
* knot_v: The old knot vector

# Returns
* C,new_knot_v
"""
function BezExtOpMatrix(new_knot::Float64,
                        order::Int64,
                        knot_v::Array{Float64})

    # number of control points
    num_cp = length(knot_v)-order-1

    #Init the new knot vector
    new_knot_v = zeros(length(knot_v)+1)

    #Insert the new knot into the knot vector
    old_i = 1
    new_i = 1
    new_knot_i = -1
    len_kv = length(knot_v)
    while(old_i <= len_kv)
        new_knot_v[new_i] = knot_v[old_i]
        if(new_knot >= knot_v[old_i] && old_i < len_kv && new_knot < knot_v[old_i+1] )
            new_i += 1
            new_knot_v[new_i] = new_knot
            new_knot_i = new_i
        end
        old_i += 1
        new_i += 1
    end

    # Init the bezExtOp Matrix
    n = num_cp
    m = num_cp + 1
    C = spzeros(n,m)

    #Fill in the new control points array
    k = new_knot_i-1
    p = order
    for i = 1:m
        # Determine alpha
        if( i >= 1 && i <= k-p )
            alpha = 1.
        elseif( i >= k-p+1 && i <= k )
            alpha = (new_knot - knot_v[i]) / (knot_v[i+p] - knot_v[i])
            if( (knot_v[i+p] - knot_v[i]) == 0 )
                println("ERROR!! denominator is zero!")
            end
        else
            alpha = 0.
        end

        # Determine the new points
        if( i == 1 )
            C[i,i] = 1.
            #C[i,i] = alpha
        elseif( i > 1 && i < m )
            C[i-1,i] = (1.-alpha)
            C[i,i] = alpha
        elseif( i == m )
            C[i-1,i] = 1.
            #C[i-1,i] = (1.-alpha)
        end
    end

    # Return the results
    return C,new_knot_v
end








"""
function InsertKnot(new_knot::Float64,
                    order::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2})
======
This function is for B-splines.

# Parameters:
* new_knot: The new knot to be inserted
* order: The order of the curve
* knot_v: The old knot vector
* control_pts: The control points of the old spline
     (dimension d-by-n, for spatial dimenion d and for n control points)

# Returns
* new_knot_v,new_control_pts
"""
function InsertKnot(new_knot::Float64,
                    order::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2})

    #Init the new knot vector
    new_knot_v = zeros(length(knot_v)+1)

    #Insert the new knot into the knot vector
    old_i = 1
    new_i = 1
    new_knot_i = -1
    len_kv = length(knot_v)
    while(old_i <= len_kv)
        new_knot_v[new_i] = knot_v[old_i]
        if(new_knot >= knot_v[old_i] && old_i < len_kv && new_knot < knot_v[old_i+1] )
            new_i += 1
            new_knot_v[new_i] = new_knot
            new_knot_i = new_i
        end
        old_i += 1
        new_i += 1
    end

    #Init the new control points array
    if(length(size(control_pts)) == 1)
        n = length(control_pts)
        m = length(control_pts)+1
        new_control_pts = zeros(m)
    else
        n = size(control_pts)[2]
        m = size(control_pts)[2]+1
        new_control_pts = zeros(size(control_pts)[1],m)
    end

    #Fill in the new control points array
    k = new_knot_i-1
    p = order
    for i = 1:m
        # Determine alpha
        if( i >= 1 && i <= k-p )
            alpha = 1.
        elseif( i >= k-p+1 && i <= k )
            alpha = (new_knot - knot_v[i]) / (knot_v[i+p] - knot_v[i])
            if( (knot_v[i+p] - knot_v[i]) == 0 )
                println("ERROR!! denominator is zero!")
            end
        else
            alpha = 0.
        end

        # Determine the new points
        if( i == 1 )
            new_control_pts[:,i] = control_pts[:,i]
        elseif( i > 1 && i < m )
            new_control_pts[:,i] = alpha*control_pts[:,i] + (1.-alpha)*control_pts[:,i-1]
        elseif( i == m )
            new_control_pts[:,i] = control_pts[:,n]
        end
    end

    # Return the results
    return new_knot_v,new_control_pts
end




"""
function InsertKnot(new_knot::Float64,
                    order::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64})
======
This function is for NURBS (includes weights).

# Parameters
* new_knot: The new knot to be inserted
* order: The order of the curve
* knot_v: The old knot vector
* control_pts: The control points of the old spline
     (dimension d-by-n, for spatial dimenion d and for n control points)
* weights: The wieghts for each NURBS control point

# Returns
new_knot_v, new_control_pts, new_weights
"""
function InsertKnot(new_knot::Float64,
                    order::Int64,
                    knot_v::Array{Float64},
                    control_pts::Array{Float64,2},
                    weights::Array{Float64})

    # Project to the higher-dimensional space.
    dim_s = size(control_pts)[1]
    n_cont_pts = size(control_pts)[2]
    nurbs_control_pts = zeros(dim_s+1,n_cont_pts)
    for row = 1:dim_s
        nurbs_control_pts[row,:] = control_pts[row,:][:].*weights[:]
    end
    nurbs_control_pts[dim_s+1,:] = weights[:]

    # Insert the knot
    new_knot_v, new_nurbs_control_pts = InsertKnot(new_knot,order,knot_v,nurbs_control_pts)

    # Convert back to NURBS
    new_weights = new_nurbs_control_pts[dim_s+1,:][:]
    new_control_pts = zeros(dim_s,size(new_nurbs_control_pts)[2])
    for row = 1:dim_s
        new_control_pts[row,:] = new_nurbs_control_pts[row,:][:] ./ new_weights[:]
    end

    # Return the result
    return new_knot_v, new_control_pts, new_weights
end









"""
Bezier type
=====

# Attributes:
* dim_p: Parameter-space dimensions
* dim_s: Spatial dimensions
* order: The polynomial order of the curve in each parameter-space direction.
         Array of integers. Example: [3,2]
* spans:  The number of spans between control points in each direction
          Array of integers. Example: [5,6]
* control_pts:  A matrix of dimension (dim_s-by-n) for n control pts, containing the
                spatial coordinates of each control point.
                For dim_p=2, the matrix is dimension (dim_s-by-n1-by-n2)
* knot_v: The array of knot vectors, for each spatial direction.
           Surface example: Array[[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]]

# Constructor:
* function Bezier(dim_p::Int64,
                dim_s::Int64,
                order::Array{Int64},
                knot_v::Array{Array{}},
                spans::Array{Int64},
                control_pts::Array{})
* function Bezier(other::Bezier)
"""
type Bezier
    dim_p::Int64
    dim_s::Int64
    order::Array{Int64}
    knot_v::Array{Array{}}
    spans::Array{Int64}
    control_pts::Array{}
    function Bezier(dim_p::Int64,
                    dim_s::Int64,
                    order::Array{Int64},
                    knot_v::Array{Array{}},
                    spans::Array{Int64},
                    control_pts::Array{})
        self = new()
        self.dim_p = copy(dim_p)
        self.dim_s = copy(dim_s)
        self.order = copy(order)
        self.spans = copy(spans)
        self.control_pts = copy(control_pts)
        self.knot_v = copy(knot_v)
        return self
    end
    function Bezier(other::Bezier)
        self = Bezier(other.dim_p,
                      other.dim_s,
                      other.order,
                      other.knot_v,
                      other.spans,
                      other.control_pts)
        return self
    end
end




"""
function domain_X(self::Bezier,t::Array{Float64,2})
========

# Parameters:
* t: Array of t values to be evaluated, shape (dim_p,n)
* Returns: The domain coordinate x for nurbs parameter t.
"""
function domain_X(self::Bezier,t::Array{Float64,2})
    result = zeros(self.dim_s,size(t)[2])
    for i = 1:size(t)[2]
        result[:,i] = NurbsDomainX(self.order,self.knot_v,self.control_pts,ones(length(self.control_pts)),t[:,i])
    end
    return result
end



"""
function draw(self::Bezier,res::Int64)
=======

# Parameters:
* self: Bezier object pointer
* res: The number of values in the array t, in each direction
      Example:  if paramter t goes from 0 to 2, and res = 4,
                then t becomes 0,0.5,1,1.5

# Returns:
* X,t,h: X is x values of curve; t is t values, h is interval in t.
"""
function draw(self::Bezier,res::Array{Int64})
    weights = nothing
    if(self.dim_p == 1)
        weights = [ones(self.spans[1]+1)]
    elseif(self.dim_p == 2)
        weights = [ones(self.spans[1]+1), ones(self.spans[2]+1)]
    elseif(self.dim_p == 3)
        weights = [ones(self.spans[1]+1), ones(self.spans[2]+1),ones(self.spans[3]+1)]
    end
    return  NurbsDraw(self.dim_p,
                      self.dim_s,
                      self.order,
                      self.knot_v,
                      self.spans,
                      self.control_pts,
                      weights,
                      res)
end





"""
function insertKnot(self::Bezier,dimension_p::Int64,new_knot::Float64)
========

# Parameters:
* self: Bezier object pointer
* dimension_p: The parameter dimension index to add the knot to.
* new_knot: The knot value to be inserted
"""
function insertKnot(self::Bezier,dimension_p::Int64,new_knot::Float64)
    new_control_pts = nothing
    new_kv = nothing
    new_cpts = nothing

    # Parameter 1D
    if(self.dim_p == 1)
        direction_p = 1
        new_control_pts = zeros(self.dim_s,
                                length(self.control_pts[1,:])+1)
        for direction_s = 1:self.dim_s
            c_pts = self.control_pts[direction_s,:]
            c_pts = reshape(c_pts,1,length(c_pts))
            new_kv,new_cpts = InsertKnot(new_knot,
                                         self.order[direction_p],
                                         self.knot_v[direction_p],
                                         c_pts)
            new_control_pts[direction_s,:] = new_cpts
        end

    # Parameter 2D
    elseif(self.dim_p == 2)

        if(direction_p == 1)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1])+1,
                                    length(self.control_pts[1,1,:]))
            for direction_s = 1:self.dim_s
                for odir = 1:length(self.control_pts[1,1,:])
                    c_pts = self.control_pts[direction_s,:,odir]
                    c_pts = reshape(c_pts,1,length(c_pts))
                    new_kv,new_cpts = InsertKnot(new_knot,
                                                 self.order[direction_p],
                                                 self.knot_v[direction_p],
                                                 c_pts)
                    new_control_pts[direction_s,:,odir] = new_cpts
                end
            end

        elseif(direction_p == 2)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1]),
                                    length(self.control_pts[1,1,:])+1)
            for direction_s = 1:self.dim_s
                for odir = 1:length(self.control_pts[1,:,1])
                    c_pts = self.control_pts[direction_s,odir,:]
                    c_pts = reshape(c_pts,1,length(c_pts))
                    new_kv,new_cpts = InsertKnot(new_knot,
                                                 self.order[direction_p],
                                                 self.knot_v[direction_p],
                                                 c_pts)
                    new_control_pts[direction_s,odir,:] = new_cpts
                end
            end
        end

    # Parameter 3D
    elseif(self.dim_p == 3)

        if(direction_p == 1)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1])+1,
                                    length(self.control_pts[1,1,:,1]),
                                    length(self.control_pts[1,1,1,:]))
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,1,:,1])
                    for odir2 = 1:length(self.control_pts[1,1,1,:])
                        c_pts = self.control_pts[direction_s,:,odir1,odir2]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts = InsertKnot(new_knot,
                                                     self.order[direction_p],
                                                     self.knot_v[direction_p],
                                                     c_pts)
                        new_control_pts[direction_s,:,odir1,odir2] = new_cpts
                    end
                end
            end

        elseif(direction_p == 2)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1]),
                                    length(self.control_pts[1,1,:,1])+1,
                                    length(self.control_pts[1,1,1,:]))
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,:,1,1])
                    for odir2 = 1:length(self.control_pts[1,1,1,:])
                        c_pts = self.control_pts[direction_s,odir1,:,odir2]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts = InsertKnot(new_knot,
                                                     self.order[direction_p],
                                                     self.knot_v[direction_p],
                                                     c_pts)
                        new_control_pts[direction_s,odir1,:,odir2] = new_cpts
                    end
                end
            end

        elseif(direction_p == 3)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1]),
                                    length(self.control_pts[1,1,:,1]),
                                    length(self.control_pts[1,1,1,:])+1)
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,:,1,1])
                    for odir2 = 1:length(self.control_pts[1,1,:,1])
                        c_pts = self.control_pts[direction_s,odir1,odir2,:]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts = InsertKnot(new_knot,
                                                     self.order[direction_p],
                                                     self.knot_v[direction_p],
                                                     c_pts)
                        new_control_pts[direction_s,odir1,odir2,:] = new_cpts
                    end
                end
            end
        end

    end

    self.knot_v[direction_p] = new_kv
    self.control_pts = new_control_pts
    self.spans[direction_p] = size(new_control_pts)[1+direction_p]-1
    return
end



"""
function basis(self::Bezier,
               i::Int64,
               t::Union{Float64,Array{Float64},FloatRange{Float64}})
=======

# Parameters:
* self: Pointer to the Bezier object
* i: knot index
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the NURBS basis evaluated at values t.
"""
function basis(self::Bezier,
               i::Int64,
               t::Union{Float64,Array{Float64},FloatRange{Float64}})
    return BezierBasis(i,self.order[1],self.knot_v[1],t)
end
function basis(self::Bezier,
               i::Array{Int64},
               t::Array{Float64})
    return BezierBasis(i,self.order,self.knot_v,t)
end




"""
function delBasis(self::Bezier,
                  i::Int64,
                  t::Union{Float64,Array{Float64},FloatRange{Float64}})
==========

# Parameters:
* self: Pointer to the Bezier object
* i: knot index
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the NURBS basis evaluated at values t.
"""
function delBasis(self::Bezier,
                  i::Int64,
                  t::Union{Float64,Array{Float64},FloatRange{Float64}})
    return DelBezierBasis(i,self.order,self.knot_v[1],t)
end




"""
function domain_dXdt(self::Bezier,
                     t::Union{Float64,Array{Float64},FloatRange{Float64}})
========

# Parameters
* self: Pointer to the Bezier object
* t: parameter value where evaluated (must be single value)

# Returns:
* The derivative of X(t) with respect to t.
"""
function domain_dXdt(self::Bezier,
                     t::Union{Float64,Array{Float64},FloatRange{Float64}})
    result = NurbsDomainDXDt(self.order[1],
                               self.knot_v[1],
                               self.control_pts,
                               ones(length(self.control_pts)),
                               t)
    return result
end





"""
Nurbs type
=====

# Attributes:
* dim_p: Parameter-space dimensions
* dim_s: Spatial dimensions
* order: The polynomial order of the curve in each parameter-space direction.
         Array of integers. Example: [3,2]
* knot_v: The array of knot vectors, for each spatial direction.
            Surface example: Array[[0,0,0,0,1,1,1,1],[0,0,0,0,1,1,1,1]]
* spans:  The number of spans between control points in each direction
          Array of integers. Example: [5,6]
* control_pts:  A matrix of dimension (dim_s-by-n) for n control pts,
          containing the spatial coordinates of each control point.
          For dim_p=2, the matrix is dimension (dim_s-by-n1-by-n2)
* weights: An array of length n, for n control points - one weight for
          each control point.

# Constructor:
* function Nurbs(dim_p::Int64,
                   dim_s::Int64,
                   order::Array{Int64},
                   knot_v,
                   spans::Array{Int64},
                   control_pts::Array{Float64,2},
                   weights::Array{Float64})
* function Nurbs(bspline::Bezier,weights::Array{Float64})
* function Nurbs(other::Nurbs)
"""
type Nurbs
    dim_p::Int64
    dim_s::Int64
    order::Array{Int64}
    knot_v::Array{Array{}}
    spans::Array{Int64}
    control_pts::Array{Float64}
    weights::Array{Array{}}
    function Nurbs(dim_p::Int64,
                   dim_s::Int64,
                   order::Array{Int64},
                   knot_v::Array{Array{}},
                   spans::Array{Int64},
                   control_pts::Array{Float64},
                   weights::Array{Array{}})
        self = new()
        self.dim_p = copy(dim_p)
        self.dim_s = copy(dim_s)
        self.order = copy(order)
        self.spans = copy(spans)
        self.control_pts = copy(control_pts)
        self.knot_v = copy(knot_v)
        self.weights = copy(weights)
        return self
    end
    function Nurbs(bspline::Bezier,weights::Array{Float64})
        self = Nurbs(bsp.dim_p, bsp.dim_s, bsp.order,
                     bsp.knot_v, bsp.spans, bsp.control_pts, weights)
        return self
    end
    function Nurbs(other::Nurbs)
        self = Nurbs(other.dim_p,
                      other.dim_s,
                      other.order,
                      other.knot_v,
                      other.spans,
                      other.control_pts,
                      other.weights)
        return self
    end
end



"""
function domain_X(self::Nurbs,t::Array{Float64,2})
======

# Parameters:
* self::Nurbs
* t: Array of values t to be evaluated, shape (dim_p,n)
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)

# Returns:
* The domain coordinate x for nurbs parameter t.
"""
function domain_X(self::Nurbs,t::Array{Float64,2})
    result = zeros(self.dim_s,size(t)[2])
    for i = 1:size(t)[2]
        result[:,i] = NurbsDomainX(self.order,self.knot_v,self.control_pts,self.weights,t[:,i])
    end
    return result
end
function domain_X(self::Nurbs,t::Array{Float64,2},cpsupport::Array{Array{}})
    result = zeros(self.dim_s,size(t)[2])
    for i = 1:size(t)[2]
        result[:,i] = NurbsDomainX(self.order,self.knot_v,self.control_pts,self.weights,t[:,i],cpsupport)
    end
    return result
end




"""
function draw(self::Nurbs,res::Array{Int64})
=======

# Parameters:
* self: Nurbs object pointer
* res: The number of values in the array t, in each direction
       Example:  if paramter t goes from 0 to 2, and res = 4,
                 then t becomes 0,0.5,1,1.5

# Returns:
* X,t,h: X is x values of curve; t is t values, h is interval in t.
"""
function draw(self::Nurbs,res::Array{Int64})
    return  NurbsDraw(self.dim_p,
                      self.dim_s,
                      self.order,
                      self.knot_v,
                      self.spans,
                      self.control_pts,
                      self.weights,
                      res)
end



"""
function insertKnot(self::Nurbs,direction_p::Int64,new_knot::Float64)
=========
Inserts a knot into the Nurbs spline

# Parameters:
* self: NURBS object pointer
* direction_p: The parameter direction to which the knot is being added.
* new_knot: The knot value to be inserted
"""
function insertKnot(self::Nurbs,direction_p::Int64,new_knot::Float64)

    new_control_pts = nothing
    new_kv = nothing
    new_cpts = nothing
    new_w = nothing

    # Parameter 1D
    if(self.dim_p == 1)
        direction_p = 1
        new_control_pts = zeros(self.dim_s,
                                length(self.control_pts[1,:])+1)
        for direction_s = 1:self.dim_s
            c_pts = self.control_pts[direction_s,:]
            c_pts = reshape(c_pts,1,length(c_pts))
            new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                               self.order[direction_p],
                                               self.knot_v[direction_p],
                                               c_pts,
                                               self.weights[direction_p])
            new_control_pts[direction_s,:] = new_cpts
        end

    # Parameter 2D
    elseif(self.dim_p == 2)

        if(direction_p == 1)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1])+1,
                                    length(self.control_pts[1,1,:]))
            for direction_s = 1:self.dim_s
                for odir = 1:length(self.control_pts[1,1,:])
                    c_pts = self.control_pts[direction_s,:,odir]
                    c_pts = reshape(c_pts,1,length(c_pts))
                    new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                                       self.order[direction_p],
                                                       self.knot_v[direction_p],
                                                       c_pts,
                                                       self.weights[direction_p])
                    new_control_pts[direction_s,:,odir] = new_cpts
                end
            end

        elseif(direction_p == 2)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1]),
                                    length(self.control_pts[1,1,:])+1)
            for direction_s = 1:self.dim_s
                for odir = 1:length(self.control_pts[1,:,1])
                    c_pts = self.control_pts[direction_s,odir,:]
                    c_pts = reshape(c_pts,1,length(c_pts))
                    new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                                       self.order[direction_p],
                                                       self.knot_v[direction_p],
                                                       c_pts,
                                                       self.weights[direction_p])
                    new_control_pts[direction_s,odir,:] = new_cpts
                end
            end
        end

    # Parameter 3D
    elseif(self.dim_p == 3)

        if(direction_p == 1)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1])+1,
                                    length(self.control_pts[1,1,:,1]),
                                    length(self.control_pts[1,1,1,:]))
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,1,:,1])
                    for odir2 = 1:length(self.control_pts[1,1,1,:])
                        c_pts = self.control_pts[direction_s,:,odir1,odir2]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                                           self.order[direction_p],
                                                           self.knot_v[direction_p],
                                                           c_pts,
                                                           self.weights[direction_p])
                        new_control_pts[direction_s,:,odir1,odir2] = new_cpts
                    end
                end
            end

        elseif(direction_p == 2)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1]),
                                    length(self.control_pts[1,1,:,1])+1,
                                    length(self.control_pts[1,1,1,:]))
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,:,1,1])
                    for odir2 = 1:length(self.control_pts[1,1,1,:])
                        c_pts = self.control_pts[direction_s,odir1,:,odir2]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                                           self.order[direction_p],
                                                           self.knot_v[direction_p],
                                                           c_pts,
                                                           self.weights[direction_p])
                        new_control_pts[direction_s,odir1,:,odir2] = new_cpts
                    end
                end
            end

        elseif(direction_p == 3)
            new_control_pts = zeros(self.dim_s,
                                    length(self.control_pts[1,:,1,1]),
                                    length(self.control_pts[1,1,:,1]),
                                    length(self.control_pts[1,1,1,:])+1)
            for direction_s = 1:self.dim_s
                for odir1 = 1:length(self.control_pts[1,:,1,1])
                    for odir2 = 1:length(self.control_pts[1,1,:,1])
                        c_pts = self.control_pts[direction_s,odir1,odir2,:]
                        c_pts = reshape(c_pts,1,length(c_pts))
                        new_kv,new_cpts,new_w = InsertKnot(new_knot,
                                                           self.order[direction_p],
                                                           self.knot_v[direction_p],
                                                           c_pts,
                                                           self.weights[direction_p])
                        new_control_pts[direction_s,odir1,odir2,:] = new_cpts
                    end
                end
            end
        end

    end

    self.knot_v[direction_p] = new_kv
    self.weights[direction_p] = new_w
    self.control_pts = new_control_pts
    self.spans[direction_p] = size(new_control_pts)[1+direction_p]-1
    return
end





"""
function basis(self::Nurbs,
               i::Int64,
               t::Union{Float64,Array{Float64},FloatRange{Float64}})
===========

# Parameters:
* self: Pointer to the Nurbs object
* i: knot index
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the NURBS basis evaluated at values t.
"""
function basis(self::Nurbs,
               i::Int64,
               t::Union{Float64,Array{Float64},FloatRange{Float64}})
    return NurbsBasis(i,self.order[1],self.knot_v[1],self.weights[1],t)
end
function basis(self::Nurbs,
               i::Array{Int64},
               t::Array{Float64})
    return NurbsBasis(i,self.order,self.knot_v,self.weights,t)
end



"""
function delBasis(self::Nurbs,
                  i::Int64,
                  t::Union{Float64,Array{Float64},FloatRange{Float64}})
=========

# Parameters:
* self: Pointer to the Nurbs object
* i: knot index
* t: Array of floats: The parameter values where the function is evaluated

# Returns:
* The value of the NURBS basis evaluated at values t.
"""
function delBasis(self::Nurbs,
                  i::Int64,
                  t::Union{Float64,Array{Float64},FloatRange{Float64}})
    return DelNurbsBasis(i,self.order[1],self.knot_v[1],self.weights,t)
end





"""
function domain_dXdt(self::Nurbs,
                     t::Union{Float64,Array{Float64},FloatRange{Float64}})
========

# Parameters:
* self: Pointer to the Bezier object
* t: Array of values t to be evaluated, shape (dim_p,n)
* cpsupport: Optional: An array of control point indicies, specifying
 the support range of value t.  Don't use if you don't know what
 that is. (improves performance)

# Returns:
* The derivative of X(t) with respect to t, for each t value
* Dimension (dim_s, dim_p, n)
"""
function domain_dXdt(self::Nurbs,t::Array{Float64,2})
    result = zeros(self.dim_s,self.dim_p,size(t)[2])
    for i = 1:size(t)[2]
        result[:,:,i] =  NurbsDomainDXDt(self.order,
                                         self.knot_v,
                                         self.control_pts,
                                         self.weights,
                                         t[:,i])
    end
    return result
end
function domain_dXdt(self::Nurbs,
                     t::Array{Float64,2},
                     cpsupport::Array{Array{}})
    result = zeros(self.dim_s,self.dim_p,size(t)[2])
    for i = 1:size(t)[2]
        result[:,:,i] = NurbsDomainDXDt(self.order,
                                        self.knot_v,
                                        self.control_pts,
                                        self.weights,
                                        t[:,i],
                                        cpsupport)
    end
    return result
end


end # module SSBezierTools
